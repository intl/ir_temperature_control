#include <iostm8s003f3.h>
#include <cstdint>
#include <iostream>
#include <intrinsics.h>
#include <cmath>

#include "lcd1202.hpp"
#include "load.hpp"
#include "btn.hpp"
#include "pid.hpp"

volatile uint32_t sys_time;

#define LOAD_TOP_HEATER 2
#define LOAD_BOTTOM_HEATER 1

#define MAX_TOP_CTRL_TEMP 300
#define MAX_BOTTOM_CTRL_TEMP 200

#define MENU_DELAY_BEFORE_EXIT 10

#define MENU_TOP_CTRL_TEMP 0
#define MENU_TOP_PWM_LVL 1
#define MENU_BOTTOM_CTRL_TEMP 2
#define MENU_BOTTOM_PWM_LVL 3

#define ADC_TOP_CH 4
#define ADC_BOTTOM_CH 3
#define ADC_DELAY 100



// �������� �����������, ������������ ���� ����� ����������� ��� ������ ������� �������� �������
#define TEMPERATURE_UNDER 3000
// �������� �����������, ������������ ���� ����� ����������� ��� ������ ���������� �������� �������
#define TEMPERATURE_OVER -550
// �������� ����������� ��������������� ������� �������� �������
#define TEMPERATURE_TABLE_START 3000
// ��� ������� 
#define TEMPERATURE_TABLE_STEP -5

// ��� ������� �������� � �������, ���� ����� ������� � �������� 16 ��� - uint16_t, ����� - uint32_t
typedef uint16_t temperature_table_entry_type;
// ��� ������� �������. ���� � ������� ������ 256 ���������, �� uint16_t, ����� - uint8_t
typedef uint16_t temperature_table_index_type;
// ����� ������� � �������� �������, ������ ��������������� temperature_table_entry_type
#define TEMPERATURE_TABLE_READ(i) termo_table[i]

/* ������� ���������� �������� ��� � ����������� �� �����������. �� �������� �������� � ��������
   ��� ���������� ������� ������������ ��������� ����������:
     R1(T1): 100���(25��)
     ������� R/T �������������: EPCOS R/T:8304; B25/100:4092K
     ����� ���������: C
     Ra: 1���
     ���������� U0/Uref: 3.3�/3.3�
*/
const temperature_table_entry_type termo_table[] = {
    1001, 1001, 1001, 1001, 1001, 1000, 1000, 1000,
    1000, 1000, 1000, 999, 999, 999, 999, 999,
    999, 998, 998, 998, 998, 998, 997, 997,
    997, 997, 997, 997, 996, 996, 996, 996,
    996, 995, 995, 995, 995, 995, 994, 994,
    994, 994, 993, 993, 993, 993, 993, 992,
    992, 992, 992, 991, 991, 991, 991, 990,
    990, 990, 990, 989, 989, 989, 989, 988,
    988, 988, 988, 987, 987, 987, 987, 986,
    986, 986, 985, 985, 985, 985, 984, 984,
    984, 983, 983, 983, 982, 982, 982, 981,
    981, 981, 980, 980, 980, 979, 979, 979,
    978, 978, 978, 977, 977, 977, 976, 976,
    976, 975, 975, 974, 974, 974, 973, 973,
    972, 972, 972, 971, 971, 970, 970, 969,
    969, 969, 968, 968, 967, 967, 966, 966,
    965, 965, 964, 964, 963, 963, 962, 962,
    961, 961, 960, 960, 959, 959, 958, 958,
    957, 957, 956, 955, 955, 954, 954, 953,
    953, 952, 951, 951, 950, 949, 949, 948,
    948, 947, 946, 946, 945, 944, 944, 943,
    942, 942, 941, 940, 939, 939, 938, 937,
    936, 936, 935, 934, 933, 933, 932, 931,
    930, 929, 929, 928, 927, 926, 925, 924,
    923, 923, 922, 921, 920, 919, 918, 917,
    916, 915, 914, 913, 912, 911, 910, 909,
    908, 907, 906, 905, 904, 903, 902, 901,
    900, 899, 898, 896, 895, 894, 893, 892,
    891, 889, 888, 887, 886, 884, 883, 882,
    881, 879, 878, 877, 875, 874, 873, 871,
    870, 869, 867, 866, 864, 863, 861, 860,
    858, 857, 855, 854, 852, 851, 849, 848,
    846, 844, 843, 841, 839, 838, 836, 834,
    833, 831, 829, 827, 825, 824, 822, 820,
    818, 816, 814, 812, 810, 809, 807, 805,
    803, 801, 799, 796, 794, 792, 790, 788,
    786, 784, 782, 779, 777, 775, 773, 771,
    768, 766, 764, 761, 759, 757, 754, 752,
    749, 747, 744, 742, 739, 737, 734, 732,
    729, 726, 724, 721, 719, 716, 713, 710,
    708, 705, 702, 699, 697, 694, 691, 688,
    685, 682, 679, 676, 673, 670, 667, 664,
    661, 658, 655, 652, 649, 646, 643, 639,
    636, 633, 630, 626, 623, 620, 617, 613,
    610, 607, 603, 600, 597, 593, 590, 586,
    583, 579, 576, 572, 569, 565, 562, 558,
    555, 551, 548, 544, 540, 537, 533, 529,
    526, 522, 518, 515, 511, 507, 504, 500,
    496, 493, 489, 485, 481, 478, 474, 470,
    466, 462, 459, 455, 451, 447, 444, 440,
    436, 432, 428, 425, 421, 417, 413, 410,
    406, 402, 398, 394, 391, 387, 383, 379,
    376, 372, 368, 364, 361, 357, 353, 350,
    346, 342, 339, 335, 331, 328, 324, 321,
    317, 313, 310, 306, 303, 299, 296, 292,
    289, 285, 282, 279, 275, 272, 268, 265,
    262, 258, 255, 252, 249, 245, 242, 239,
    236, 233, 230, 227, 223, 220, 217, 214,
    211, 208, 205, 203, 200, 197, 194, 191,
    188, 186, 183, 180, 177, 175, 172, 169,
    167, 164, 162, 159, 157, 154, 152, 149,
    147, 145, 142, 140, 138, 136, 133, 131,
    129, 127, 125, 123, 120, 118, 116, 114,
    112, 110, 108, 107, 105, 103, 101, 99,
    97, 96, 94, 92, 91, 89, 87, 86,
    84, 82, 81, 79, 78, 76, 75, 73,
    72, 71, 69, 68, 67, 65, 64, 63,
    61, 60, 59, 58, 57, 56, 54, 53,
    52, 51, 50, 49, 48, 47, 46, 45,
    44, 43, 42, 41, 40, 40, 39, 38,
    37, 36, 35, 35, 34, 33, 32, 32,
    31, 30, 30, 29, 28, 28, 27, 26,
    26, 25, 25, 24, 23, 23, 22, 22,
    21, 21, 20, 20, 19, 19, 18, 18,
    17, 17, 17, 16, 16, 15, 15, 15,
    14, 14, 14, 13, 13, 13, 12, 12,
    12, 11, 11, 11, 10, 10, 10, 10,
    9, 9, 9, 9, 8, 8, 8, 8,
    8, 7, 7, 7, 7, 7, 6, 6,
    6, 6, 6, 6, 5, 5, 5, 5,
    5, 5, 5, 4, 4, 4, 4, 4,
    4, 4, 4, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 0, 0
};

// ������� ��������� �������� ����������� � ������� ����� �������� �������
// � ����������� �� ���������� �������� ���.
int16_t calc_temperature(temperature_table_entry_type adcsum) {
  temperature_table_index_type l = 0;
  temperature_table_index_type r = (sizeof(termo_table) / sizeof(termo_table[0])) - 1;
  temperature_table_entry_type thigh = TEMPERATURE_TABLE_READ(r);
  
  // �������� ������ �� ������� � ��������� ��������
  if (adcsum <= thigh) {
    #ifdef TEMPERATURE_UNDER
      if (adcsum < thigh) 
        return TEMPERATURE_UNDER;
    #endif
    return (int16_t)(TEMPERATURE_TABLE_STEP * (int16_t)r + TEMPERATURE_TABLE_START);
  }
  temperature_table_entry_type tlow = TEMPERATURE_TABLE_READ(0);
  if (adcsum >= tlow) {
    #ifdef TEMPERATURE_OVER
      if (adcsum > tlow)
        return TEMPERATURE_OVER;
    #endif
    return TEMPERATURE_TABLE_START;
  }

  // �������� ����� �� �������
  while ((r - l) > 1) {
    temperature_table_index_type m = (l + r) >> 1;
    temperature_table_entry_type mid = TEMPERATURE_TABLE_READ(m);
    if (adcsum > mid) {
      r = m;
    } else {
      l = m;
    }
  }
  temperature_table_entry_type vl = TEMPERATURE_TABLE_READ(l);
  if (adcsum >= vl) {
    return (int16_t)l * TEMPERATURE_TABLE_STEP + TEMPERATURE_TABLE_START;
  }
  temperature_table_entry_type vr = TEMPERATURE_TABLE_READ(r);
  temperature_table_entry_type vd = vl - vr;
  int16_t res = TEMPERATURE_TABLE_START + (int16_t)r * TEMPERATURE_TABLE_STEP; 
  if (vd) {
    // �������� ������������
    res -= ((TEMPERATURE_TABLE_STEP * (int32_t)(adcsum - vr) + (vd >> 1)) / vd);
  }
  return res;
}


union config_t
{
  struct 
  {
    uint16_t top_ctrl_temp;
    uint8_t top_pwm_level;
    uint16_t bottom_ctrl_temp;
    uint8_t bottom_pwm_level;
    uint8_t is_config_valid;
    uint8_t :8;
  };
  uint8_t raw[8];
};

__no_init config_t config @0x4000;

LCD_T lcd;
LOAD_T load;
BUTTONS_T btn;
PID_t pid_top_heater;
PID_t pid_bottom_heater;

struct heater_trigers_t
{
  uint8_t top:1;
  uint8_t bottom:1;
  uint8_t:6;
};

volatile uint16_t temp_top_heater,temp_bottom_heater,adc_delay;
heater_trigers_t  triggers;

extern "C"
{
  void adc_maintance()
  {
    if((adc_delay==ADC_DELAY))
      {   
      //������ ����� ��� ���������� ��������
      ADC_CR1_ADON = 1; 
      adc_delay = 0;
      }
    else
      {
      //������ ����� ��� tStab (������������ ���)
      if(adc_delay==(ADC_DELAY-10))
        {
        ADC_CR1_ADON = 1; 
        }
      adc_delay++;
      }
  }
  
//���������� ������� ������ 1/490 ���.   
#pragma vector=TIM4_OVR_UIF_vector
  __interrupt __root  void TIM4_OVR(void)
    {
    btn.maintance_cb();
    adc_maintance();
    sys_time++;
    TIM4_SR_bit.UIF = 0;
    }
  
 #pragma vector=TIM2_CAPCOM_CC1IF_vector
  __interrupt __root  void TIM2_CC1IR_ISR(void)
  { 
  if(TIM2_SR1_bit.CC1IF&&TIM2_IER_bit.CC1IE)
    {
    load.set(load.get()|LOAD_TOP_HEATER);
    TIM2_SR1_bit.CC1IF = 0;
    }
  if(TIM2_SR1_bit.CC2IF&&TIM2_IER_bit.CC2IE)
    {
    load.set(load.get()|LOAD_BOTTOM_HEATER);
    TIM2_SR1_bit.CC2IF = 0;
    }
  }
  
 #pragma vector=TIM2_OVR_UIF_vector  
  __interrupt __root  void TIM2_OVF(void)
  {
   load.set(load.get()& ~LOAD_TOP_HEATER);
   load.set(load.get()& ~LOAD_BOTTOM_HEATER);
   
     
   TIM2_SR1_bit.UIF = 0;
  } 
  
  
  #pragma vector = ADC1_EOC_vector
__interrupt void ADC1_EOC_IRQHandler()
{
    unsigned char low, high;
    ADC_CR1_ADON = 0;       //  Disable the ADC.
    ADC_CSR_EOC = 0;        //     Indicate that ADC conversion is complete.

    low = ADC_DRL;            //    Extract the ADC reading.
    high = ADC_DRH;
    uint16_t temp = calc_temperature(low | (high <<8))/10;
    
    if(ADC_CSR_CH == ADC_TOP_CH)
      {
      if(temp <= MAX_TOP_CTRL_TEMP)
        {
        temp_top_heater = temp;
        ADC_CSR_CH = ADC_BOTTOM_CH; 
        
        if(triggers.top)
          {
          int16_t lvl = pid_top_heater.controller(config.top_ctrl_temp,(temp_top_heater));
          
          if(config.top_ctrl_temp<(temp_top_heater))
              pid_top_heater.reset_integrator();
            
          if(lvl>config.top_pwm_level)
             lvl = config.top_pwm_level;
          if(lvl<0)
             TIM2_IER_CC1IE = 0;
          else
             TIM2_IER_CC1IE = 1;
         
          TIM2_CCR1H = 0;
          TIM2_CCR1L =  (((100-lvl)*255)/100);
          }
        else
          pid_top_heater.reset_integrator();
        return;
        }  
      }
    
    if(ADC_CSR_CH == ADC_BOTTOM_CH)
      {
      if(temp <= MAX_BOTTOM_CTRL_TEMP)
        {
        temp_bottom_heater = temp;
        ADC_CSR_CH = ADC_TOP_CH;
              
        if(triggers.bottom)
          {
          int16_t lvl = pid_bottom_heater.controller(config.bottom_ctrl_temp,(temp_bottom_heater));
          if(lvl>config.bottom_pwm_level)
            lvl = config.bottom_pwm_level;
          
          if(config.bottom_ctrl_temp<(temp_bottom_heater))
            pid_bottom_heater.reset_integrator();
          if(lvl<0)
            TIM2_IER_CC2IE = 0;
          else
            TIM2_IER_CC2IE = 1;
           
            
          TIM2_CCR2H = 0;
          TIM2_CCR2L =  (((100-lvl)*255)/100);
          }
        else
          pid_bottom_heater.reset_integrator();
        
        return;
        }
     }
      
}


//������������� ������������   
  static void sys_cfg(void)
    {
    //clock
    CLK_PCKENR1 = 0;
    CLK_PCKENR2 = 0;
    CLK_CKDIVR = 0;
    CLK_ICKR_bit.HSIEN = 1;
    CLK_ECKR_bit.HSEEN = 0;
    while(!CLK_ICKR_bit.HSIRDY);
    CLK_SWR = 0xe1;
    
    //tim4 for sys_clock
    //tim4 -> 1mhz
    CLK_PCKENR1 |= 0x10;
    TIM4_SR_bit.UIF = 0;
    TIM4_PSCR = 7;
    TIM4_ARR = 0xff;
    TIM4_IER_bit.UIE = 1;
    __enable_interrupt();
    TIM4_CR1_bit.CEN = 1;  
    
    //tim2 for PWM out
    CLK_PCKENR1 |= 0x20;
    TIM2_PSCR = 0xd;       //  Prescaler = 32.
    TIM2_ARRH = 0x0;       
    TIM2_ARRL = 0xff;       //����������� �������� �������� = 255
    
    TIM2_CCR1H = 0;      //������ �������� �����������
    TIM2_CCR1L = 0;
    TIM2_CCR2H = 0;      //������ ������� �����������
    TIM2_CCR2L = 0;
    
    TIM2_IER_UIE = 1;
   
    TIM2_CR1_CEN = 1;       //  Finally enable the timer.
    
    // ADC 
    CLK_PCKENR2 |= 0x8;
    //ADC_CR1_ADON = 1;       //  Turn ADC on, note a second set is required to start the conversion.
    ADC_CR1 |= 0x70;
    ADC_CSR_CH = ADC_TOP_CH;      //  Protomodule uses STM8S105 - no AIN4.
    ADC_CR3_DBUF = 0;
    ADC_CR2_ALIGN = 1;      //  Data is right aligned.
    ADC_CSR_EOCIE = 1;      //  Enable the interrupt after conversion completed.  
    }
}


void show_main_window()
{
  uint8_t buf[4] = {0};
  lcd.clear();
  
  // top!
  lcd.position(0,0);
  lcd.inverse(1);
  lcd.puts("     * Top *     ",16);
  lcd.inverse(0);
  lcd.position(1,1);
  lcd.puts("Toff=",5);
  lcd.itoa(config.top_ctrl_temp,buf);
  lcd.puts((char*)buf,3);
  lcd.position(2,1);
  lcd.puts("PWM =",5);
  lcd.itoa(config.top_pwm_level,buf);
  lcd.puts((char*)buf,3);
    
  //bottom
  lcd.position(4,0);
  lcd.inverse(1);
  lcd.puts("   * Bottom *   ",16);
  lcd.inverse(0);
  lcd.position(5,1);
  lcd.puts("Toff=",5);
  lcd.itoa(config.bottom_ctrl_temp,buf);
  lcd.puts((char*)buf,3);
  lcd.position(6,1);
  lcd.puts("PWM =",5);
  lcd.itoa(config.bottom_pwm_level,buf);
  lcd.puts((char*)buf,3);
}

void menu_window()
{
  uint8_t menu_was_drawed = 0;
  uint8_t position = 0;
  sys_time = 0;
  while(sys_time < (MENU_DELAY_BEFORE_EXIT*490))
    {
    switch(position)
      {
      case MENU_TOP_CTRL_TEMP:
        {
        if(!menu_was_drawed)
          {
          lcd.clear();
          lcd.position(0,0);
          lcd.inverse(1);
          lcd.puts("Top temperature ",16);
          lcd.inverse(0);
          lcd.position(7,0);
          lcd.puts(" M    + -    e  ",16);
          lcd.position(3,5);
          lcd.put_num(config.top_ctrl_temp);
       
          menu_was_drawed = 1;
          }
        
        switch(btn.get_status())
            {
            case BUTTONS_T::SEL: position=MENU_TOP_PWM_LVL; sys_time = 0; menu_was_drawed = 0; break;
            case BUTTONS_T::PLUS:
              {
              sys_time = 0;
              FLASH_DUKR = 0xAE;
              FLASH_DUKR = 0x56; 
              config.top_ctrl_temp++;
  
              if(config.top_ctrl_temp>MAX_TOP_CTRL_TEMP)
                config.top_ctrl_temp = 25;
              FLASH_DUKR = 0x56;
              FLASH_DUKR = 0xAE; 
              
              lcd.position(3,5);
              lcd.put_num(config.top_ctrl_temp);
              };break;
            case BUTTONS_T::MINUS:
              {
              sys_time = 0;
              FLASH_DUKR = 0xAE;
              FLASH_DUKR = 0x56; 
              config.top_ctrl_temp--;
              
              if(config.top_ctrl_temp<=25)
                config.top_ctrl_temp = MAX_TOP_CTRL_TEMP;
               
              FLASH_DUKR = 0x56;
              FLASH_DUKR = 0xAE; 
              lcd.position(3,5);
              lcd.put_num(config.top_ctrl_temp);
              };break;
            case BUTTONS_T::EXIT:
              {
                return;
              };break;
            }
        };break;
      case MENU_TOP_PWM_LVL:
        {
        if(!menu_was_drawed)
          {
          lcd.clear();
          lcd.position(0,0);
          lcd.inverse(1);
          lcd.puts("Top PWM Level   ",16);
          lcd.inverse(0);
          lcd.position(7,0);
          lcd.puts(" M    + -    e  ",16);
        lcd.position(3,5);
              lcd.put_num(config.top_pwm_level);
          menu_was_drawed = 1;
          }
          
          switch(btn.get_status())
            {
            case BUTTONS_T::SEL: position=MENU_BOTTOM_CTRL_TEMP; sys_time = 0; menu_was_drawed = 0; break;
            case BUTTONS_T::PLUS:
              {
              sys_time = 0;
              FLASH_DUKR = 0xAE;
              FLASH_DUKR = 0x56; 
              
              if((config.top_pwm_level+1)>100)
                config.top_pwm_level = 1;
              else
                config.top_pwm_level++;
              
              FLASH_DUKR = 0x56;
              FLASH_DUKR = 0xAE; 
              lcd.position(3,5);
              lcd.put_num(config.top_pwm_level);
              };break;
            case BUTTONS_T::MINUS:
              {
              sys_time = 0;
              FLASH_DUKR = 0xAE;
              FLASH_DUKR = 0x56; 
              
              if(config.top_pwm_level!=1)
                config.top_pwm_level--;
              else
                config.top_pwm_level = 100;
              
              FLASH_DUKR = 0x56;
              FLASH_DUKR = 0xAE; 
              lcd.position(3,5);
              lcd.put_num(config.top_pwm_level);
              };break;
            case BUTTONS_T::EXIT:
              {
                return;
              };break;
            }
          
        };break;
      case MENU_BOTTOM_CTRL_TEMP:
        {
        if(!menu_was_drawed)
          {
          lcd.clear();
          lcd.position(0,0);
          lcd.inverse(1);
          lcd.puts("Bottom temp.    ",16);
          lcd.inverse(0);
          lcd.position(7,0);
          lcd.puts(" M    + -    e  ",16);
            lcd.position(3,5);
          lcd.put_num(config.bottom_ctrl_temp);
          menu_was_drawed = 1;
          }
        
          switch(btn.get_status())
            {
            case BUTTONS_T::SEL: position=MENU_BOTTOM_PWM_LVL; sys_time = 0; menu_was_drawed = 0;break;
            case BUTTONS_T::PLUS:
              {
              
              sys_time = 0;
              FLASH_DUKR = 0xAE;
              FLASH_DUKR = 0x56; 
              config.bottom_ctrl_temp++;
                  
              if(config.bottom_ctrl_temp>MAX_BOTTOM_CTRL_TEMP)
                config.bottom_ctrl_temp = 25;
              
              FLASH_DUKR = 0x56;
              FLASH_DUKR = 0xAE; 
                lcd.position(3,5);
          lcd.put_num(config.bottom_ctrl_temp);
              };break;
            case BUTTONS_T::MINUS:
              {
              sys_time = 0;
              FLASH_DUKR = 0xAE;
              FLASH_DUKR = 0x56; 
              config.bottom_ctrl_temp--;
              
              if(config.bottom_ctrl_temp<=25)
                config.bottom_ctrl_temp = MAX_BOTTOM_CTRL_TEMP;
              
              FLASH_DUKR = 0x56;
              FLASH_DUKR = 0xAE; 
              lcd.position(3,5);
              lcd.put_num(config.bottom_ctrl_temp);
              };break;
            case BUTTONS_T::EXIT:
              {
                return;
              };break;
            }
        };break;
      case MENU_BOTTOM_PWM_LVL:
        {
        
        if(!menu_was_drawed)
          {
          lcd.clear();
          lcd.position(0,0);
          lcd.inverse(1);
          lcd.puts("Bottom PWM Level",16);
          lcd.inverse(0);
          lcd.position(7,0);
          lcd.puts(" M    + -    e  ",16);
          lcd.position(3,5);
          lcd.put_num(config.bottom_pwm_level);
          menu_was_drawed = 1;
          }        
        switch(btn.get_status())
            {
            case BUTTONS_T::SEL:menu_was_drawed = 0; position=0; sys_time = 0; break;
            case BUTTONS_T::PLUS:
              {
           
              sys_time = 0;
              FLASH_DUKR = 0xAE;
              FLASH_DUKR = 0x56; 
              
              if((config.bottom_pwm_level+1)>100)
                config.bottom_pwm_level = 1;
              else
                config.bottom_pwm_level++;
              
              FLASH_DUKR = 0x56;
              FLASH_DUKR = 0xAE; 
              
              lcd.position(3,5);
              lcd.puts("          ",10);
              lcd.position(3,5);
              lcd.put_num(config.bottom_pwm_level);
              
              };break;
            case BUTTONS_T::MINUS:
              {

              sys_time = 0;
              FLASH_DUKR = 0xAE;
              FLASH_DUKR = 0x56; 
              
              if(config.bottom_pwm_level!=1)
                config.bottom_pwm_level--;
              else
                config.bottom_pwm_level = 100;

              
              FLASH_DUKR = 0x56;
              FLASH_DUKR = 0xAE;
              
              lcd.position(3,5);
              lcd.put_num(config.bottom_pwm_level);
              };break;
            case BUTTONS_T::EXIT:
              {
                return;
              };break;
            }
        };break;
      default:
        {
           menu_was_drawed = 0;position=0;
        }
      }
    }
}


int main()
{
  sys_cfg();
  lcd.init();
  lcd.backlight_on();
  
  if((!config.is_config_valid)||(btn.get_status()==BUTTONS_T::EXIT))
    {
    lcd.puts("Defaults loaded",15);
    FLASH_DUKR = 0xAE;
    FLASH_DUKR = 0x56; 
    config.top_ctrl_temp = 240;
    config.top_pwm_level = 100;
    config.bottom_ctrl_temp = 150;
    config.bottom_pwm_level = 100;
    config.is_config_valid = 1;
    FLASH_DUKR = 0x56;       
    FLASH_DUKR = 0xAE;    
    while(sys_time<500);
    sys_time = 0;
    }
  
  show_main_window();
  

  while(1)   
  {
      switch(btn.get_status())
            {
            case BUTTONS_T::SEL:
              {
              menu_window();
              show_main_window();
              }; break;
              
           case BUTTONS_T::PLUS:
              {
              TIM2_IER_CC1IE = ~TIM2_IER_CC1IE;       //��������� ���������� �������� ����������
              triggers.top = TIM2_IER_CC1IE;
              
              pid_top_heater.reset_integrator();
              show_main_window();
              }; break;
           case BUTTONS_T::MINUS:
              {
              TIM2_IER_CC2IE = ~TIM2_IER_CC2IE;       //��������� ���������� �������� ����������
              triggers.bottom = TIM2_IER_CC2IE;
              pid_bottom_heater.reset_integrator();
              
              show_main_window();
              };break;
            
           case BUTTONS_T::EXIT:
              {
              TIM2_IER_CC2IE = 0; 
              TIM2_IER_CC1IE = 0; 
              triggers.top = 0;
              triggers.bottom = 0;
              show_main_window();
              pid_bottom_heater.reset_integrator();
              pid_top_heater.reset_integrator();
              };break;
              
            default:
              {
              if(sys_time>50)
                {
                uint8_t buf[4] = {0};
                lcd.position(1,10);
                lcd.puts("T=",2);
                lcd.itoa(temp_top_heater,buf,4);
                lcd.puts((char*)buf,4);
                lcd.position(2,10);
                if(triggers.top)
                  lcd.puts("ON ",2);
                else
                  lcd.puts("- ",2);
                
                lcd.position(2,13);
                if(TIM2_IER_CC1IE) 
                  lcd.puts("ON",2);
                else
                  lcd.puts("- ",2);
                
                lcd.position(5,10);
                lcd.puts("T=",2);
                lcd.itoa(temp_bottom_heater,buf,4);
                lcd.puts((char*)buf,4);
                lcd.position(6,10);
                if(triggers.bottom)
                  lcd.puts("ON",2);
                else
                  lcd.puts("- ",2);
                
                lcd.position(6,13);
                if(TIM2_IER_CC2IE) 
                  lcd.puts("ON",2);
                else
                  lcd.puts("- ",2);
                
                }
              };        
            } 
    }
  
}
