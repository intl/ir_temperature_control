#include <cstdint>
#include <cstdlib>
#include <iostm8s003f3.h>

// ��������� ��������� ������
#define LOAD_DDR PA_DDR
#define LOAD_ODR PA_ODR
#define LOAD_CR1 PA_CR1
#define LOAD_CR2 PA_CR2


class LOAD_T
{
  public:
    //�����������
    LOAD_T()
    {
      LOAD_DDR |= (1<<O0_PIN)|(1<<O1_PIN)|(1<<O2_PIN);
      LOAD_CR1 |= (1<<O0_PIN)|(1<<O1_PIN)|(1<<O2_PIN);
      
    };
    
    //������� ��������� ��������� ��������
    void set(uint8_t x);
    //������� ��������� ��������� ��������
    uint8_t get();
    
  private:
    // ��� ������ 0
    const static uint8_t O0_PIN = 1;
    // ��� ������ 1
    const static uint8_t O1_PIN = 2;
    // ��� ������ 2
    const static uint8_t O2_PIN = 3;
};