#include <cstdint>
#include <cstdlib>
#include <iostm8s003f3.h>

// ��������� ��������� ������
#define BTN_SEL_DDR PD_DDR
#define BTN_SEL_IDR PD_IDR

#define BTN_EXIT_DDR PD_DDR
#define BTN_EXIT_IDR PD_IDR

#define BTN_PLUS_DDR PB_DDR
#define BTN_PLUS_IDR PB_IDR

#define BTN_MINUS_DDR PB_DDR
#define BTN_MINUS_IDR PB_IDR

class BUTTONS_T
{
public:
  //�����������, ��� ����� �� ����, ������ ��.
  BUTTONS_T()
  {
    BTN_SEL_DDR &= ~(1<<SEL_PIN);
    BTN_PLUS_DDR &= ~(1<<PLUS_PIN);
    BTN_MINUS_DDR &= ~(1<<MINUS_PIN);
    BTN_EXIT_DDR &= ~(1<<EXIT_PIN);
    BUTTONS_T::status = NONE;
  }
  //��������� ������, �� ���� �� ������, �����, ����, �����
  enum status_ret {NONE,SEL,PLUS,SEL_PLUS,MINUS,MINUS_SEL,MINUS_PLUS,MINUS_SEL_PLUS,EXIT};
  //��������� ������
  status_ret status;
  // ������� ������� ��������� ������ � ��������� ��������� (status)
  void maintance_cb();
  // ������� ������� ���������� � ������� ������ ������
  status_ret get_status();
private:
  static const uint8_t EXIT_PIN = 5;
  //����� ���� ������ SEL
  static const uint8_t SEL_PIN = 6;
  //����� ���� ������ PLUS
  static const uint8_t PLUS_PIN = 5;
  //����� ���� ������ MINUS
  static const uint8_t MINUS_PIN = 4;
  //����� �������� ����� �������� ������ ����� ������ �������� (~200ms)
  static const uint8_t DELAY_AFTER_PRESS = 96;
  
  //������� �������� ������� ���� ������� ���� ���� ������ ������
  volatile uint16_t delay_after_press_cnt;
};