#include "lcd1202.hpp"
#include "font.h"
#include "fnt_gost_a.xbm"

/* hw undepended functions */
static const uint32_t pow10[] = {1,10,100,1000,10000,100000,1000000,10000000,100000000,1000000000};

void LCD_T::init(void)
{
    init_gpio();
    reset();
    write_byte(0xE2, 0); // Reset
    delay_ms(10);
    write_byte(0xA4, 0); // Power saver off
    write_byte(0x2F, 0); // Power control set
    write_byte(0xAF, 0); // LCD display on
    clear();
    flags.inverse = 0;
}

void LCD_T::clear(void)
  {
  int i;
  home();
  for(i=0; i<16*6*9; i++)
      {
      write_byte(0x00, 1);
      }
  }


void LCD_T::position(uint8_t row, uint8_t col)
{
    set_row(row);
    set_col(col*6);
}

void LCD_T::putc(uint8_t c)
{
  uint8_t line;
  uint8_t ch;
  for(line=0;line<6;line++)
    {
    ch = font6_8[c-32][line];
    if(flags.inverse) ch = ch ^ 0xFF;
      write_byte(ch, 1);
    }    
  cur_col+=6;
}

void LCD_T::itoa(uint32_t num, uint8_t* out,uint8_t num_size)
{
  uint8_t num_not_null = 0;
  num_size--;
  uint8_t out_size = num_size;
  do
    {
    uint8_t n = num / pow10[num_size];
    num -= n * pow10[num_size];
    if(((n)&&(n<=9))||num_not_null||!num_size)
      {
      num_not_null = 1;
      out[out_size-num_size]=n+0x30;
      }
    else
      out[out_size-num_size]=0x20;    
    }
  while(num_size--);
}

void LCD_T::put_num(uint32_t num)
{ 
uint8_t num_not_null = 0;
uint8_t num_size = 8;

for(uint8_t n = 0;n!=num_size;n++)
  {
  write_byte(0x10 | ((cur_col+(n*11))>>4), 0); // Sets the DDRAM column address - upper 3-bit
  write_byte(0x00 | ((cur_col+(n*11)) & 0x0F), 0); // lower 4-bit
  write_byte(0xB0 | (cur_row & 0x0F), 0);
  for(uint16_t line=0;line!=11;line++)
         write_byte(0, 1);
      
  write_byte(0x10 | ((cur_col+(n*11))>>4), 0); // Sets the DDRAM column address - upper 3-bit
  write_byte(0x00 | ((cur_col+(n*11)) & 0x0F), 0); // lower 4-bit
  write_byte(0xB0 | (cur_row+1 & 0x0F), 0);
  for(uint16_t line=0;line!=11;line++)
         write_byte(0, 1);
  }
do
  {
  uint8_t n = num / pow10[num_size];
  num -= n * pow10[num_size];
  if(((n)&&(n<=9))||num_not_null||!num_size)
     {
     num_not_null = 1;     
     n = fnt_gost_a_conv[n];     
     if((cur_col+11)>95)
       {
       cur_row+=2;
       cur_col = 0;
       }

     set_col(cur_col);
     set_row(cur_row);
     for(uint16_t line=(22*n);line<((22*n)+22);line+=2)
       write_byte(fnt_gost_a_bits[line], 1);
     
     set_col(cur_col);
     set_row(cur_row+1);
     
     for(uint16_t line=((22*n)+1);line<((22*n)+23);line+=2)
          write_byte(fnt_gost_a_bits[line], 1);
     
     cur_row--;        
     cur_col+=11;
    }
  }
while(num_size--);
}

void LCD_T::puts(char* str, uint8_t len)
{
  for(uint8_t i = 0;i!=len;i++)
      putc(str[i]);
}


void LCD_T::inverse(uint8_t flag)
{
    flags.inverse = flag;
}


void LCD_T::reverse(uint8_t inv)
{
    if(inv)
        write_byte(0xA7, 0); // reverse display
    else
        write_byte(0xA6, 0); // normal display
}


void LCD_T::start_line(uint8_t line)
{
    write_byte(0x40 | (line & 0x3F), 0);
}

void LCD_T::home(void)
{
    write_byte(0xB0, 0); // Page address set
    write_byte(0x10, 0); // Sets the DDRAM column address - upper 3-bit
    write_byte(0x00, 0); // lower 4-bit
}

void LCD_T::set_row(uint8_t row)
{
    cur_row = row;
    write_byte(0xB0 | (row & 0x0F), 0); // Page address set
}

void LCD_T::set_col(uint8_t col)
{
    cur_col = col;
    write_byte(0x10 | (col>>4), 0); // Sets the DDRAM column address - upper 3-bit
    write_byte(0x00 | (col & 0x0F), 0); // lower 4-bit
}

#define lcd_rst_mark() LCD_PORT &= ~LCD_NRST
#define lcd_rst_release() LCD_PORT |= LCD_NRST

#define lcd_cs_mark() LCD_PORT &= ~LCD_NCS
#define lcd_cs_release() LCD_PORT |= LCD_NCS

void LCD_T::delay_ms(uint16_t ms)
{
  while(--ms)
  {
    for(uint8_t i =0; i!=100; i++)
    {
      _delay_ten_us();
    }
  }
}

void LCD_T::_delay_ten_us()
{
  volatile uint8_t j=0;
  j++;
}

void LCD_T::init_gpio(void)
{
  LCD_DDR(LCD_NRST|LCD_NCS|LCD_SDI|LCD_SCK|LCD_BACKLIGHT);
  LCD_PORT |= LCD_NRST|LCD_NCS;
}

void LCD_T::reset(void)
{
    lcd_rst_mark();
    delay_ms(10);
    lcd_rst_release();
    delay_ms(10);
}

void LCD_T::write_byte(uint8_t data, uint8_t rc)
{
    lcd_cs_mark();
    if(rc) LCD_PORT|=LCD_SDI;
      else LCD_PORT&=~LCD_SDI;
    
    LCD_PORT|=LCD_SCK;
	_delay_ten_us();
    LCD_PORT&=~LCD_SCK;
	_delay_ten_us();
	
    int i;
    for(i=0; i<8; i++)
    {
        if(data & 0x80) 
          LCD_PORT|=LCD_SDI;
        else 
          LCD_PORT&=~LCD_SDI;
        data <<= 1;
		
	LCD_PORT|=LCD_SCK;
	_delay_ten_us();
	LCD_PORT&=~LCD_SCK;
	_delay_ten_us();
    }
    lcd_cs_release();
}

void LCD_T::backlight_on(void)
{
  LCD_PORT |= LCD_BACKLIGHT;
}

void LCD_T::backlight_off(void)
{
  LCD_PORT &= ~LCD_BACKLIGHT;
}

