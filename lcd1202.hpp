#include "iostm8s003f3.h"
#include <cstdint>
#include <cstdlib>

#define LCD_NRST        (1<<3)
#define LCD_NCS         (1<<4)
#define LCD_SDI         (1<<5)
#define LCD_SCK         (1<<6)
#define LCD_BACKLIGHT   (1<<7)
#define LCD_PORT        PC_ODR
#define LCD_DDR(X)      { PC_DDR = X; PC_CR1 = X; PC_CR2 = X; }

class LCD_T
{
public:
  //������������� �������
  void init(void);
  // ������� �������
  void clear(void);
  //
  void home(void);
  //
  void start_line(uint8_t line);
  //
  void backlight_off(void);
  void backlight_on(void);
  //
  void position(uint8_t row, uint8_t col);
  //
  void putc(uint8_t c);
  void puts(char* str,uint8_t len);
  
  void put_num(uint32_t num);
  //
  void inverse(uint8_t flag);
  void reverse(uint8_t inv);
  //
  void itoa(uint32_t num, uint8_t* out,uint8_t num_size=3);
    
  LCD_T()
  {
    cur_row = 0;
    cur_col = 0;
  };
  
private:
  //
  void init_gpio(void);
  //
  void reset(void);
  //
  void write_byte(uint8_t data, uint8_t rc);
  //
  void set_row(uint8_t row);
  void set_col(uint8_t col);
  //
  void delay_ms(uint16_t ms);
  
  void _delay_ten_us();
  
  struct flags_t{
    uint8_t inverse:1;
  };
  
  uint8_t cur_row;
  uint8_t cur_col;
  
  
  flags_t flags;
};
