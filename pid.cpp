#include "pid.hpp"


int16_t PID_t::controller(int16_t setPoint, int16_t processValue)
{
   int16_t error, p_term, d_term;
  int32_t i_term, ret, temp;

  error = setPoint - processValue;

  // Calculate Pterm and limit error overflow
  if (error > pid.maxError){
    p_term = MAX_INT;
  }
  else if (error < -pid.maxError){
    p_term = -MAX_INT;
  }
  else{
    p_term = pid.P_Factor * error;
  }

  // Calculate Iterm and limit integral runaway
  temp = pid.sumError + error;
  if(temp > pid.maxSumError){
    i_term = MAX_I_TERM;
    pid.sumError = pid.maxSumError;
  }
  else if(temp < -pid.maxSumError){
    i_term = -MAX_I_TERM;
    pid.sumError = -pid.maxSumError;
  }
  else{
    pid.sumError = temp;
    i_term = pid.I_Factor * pid.sumError;
  }

  // Calculate Dterm
  d_term = pid.D_Factor * (pid.lastProcessValue - processValue);

  pid.lastProcessValue = processValue;

  ret = (p_term + i_term + d_term) / SCALING_FACTOR;
  if(ret > MAX_INT){
    ret = MAX_INT;
  }
  else if(ret < -MAX_INT){
    ret = -MAX_INT;
  }
  return((int16_t)ret);
}

void PID_t::reset_integrator()
{
  pid.sumError = 0;
}