#include "btn.hpp"

void BUTTONS_T::maintance_cb()
{
  if(delay_after_press_cnt)
    {
      delay_after_press_cnt --;
    }
  else
    {
    status = NONE;
    
    if(!(BTN_SEL_IDR&(1<<SEL_PIN)))
      status = SEL;
    
    if(!(BTN_PLUS_IDR&(1<<PLUS_PIN)))
      status = PLUS;
    
    if((!(BTN_SEL_IDR&(1<<SEL_PIN)))&&(!(BTN_PLUS_IDR&(1<<PLUS_PIN))))
      status = SEL_PLUS;
    
    if(!(BTN_MINUS_IDR&(1<<MINUS_PIN)))
      status = MINUS;//100
    
    if((!(BTN_SEL_IDR&(1<<SEL_PIN)))&&(!(BTN_MINUS_IDR&(1<<MINUS_PIN))))
      status = MINUS_SEL; //101
    
    if((!(BTN_PLUS_IDR&(1<<PLUS_PIN)))&&(!(BTN_MINUS_IDR&(1<<MINUS_PIN))))
      status = MINUS_PLUS; //110
    
    if((!(BTN_SEL_IDR&(1<<SEL_PIN)))&&(!(BTN_MINUS_IDR&(1<<MINUS_PIN)))&&(!(BTN_PLUS_IDR&(1<<PLUS_PIN))))
      status = MINUS_SEL_PLUS; //111
    
    if(!(BTN_EXIT_IDR&(1<<EXIT_PIN)))
      status = EXIT;
        
    if(status!=NONE)
      delay_after_press_cnt = DELAY_AFTER_PRESS;
    }
}

BUTTONS_T::status_ret BUTTONS_T::get_status()
{
  status_ret s = status;
  status = NONE;
  return s;
}
