#include "load.hpp"

void LOAD_T::set(uint8_t x)
{
LOAD_ODR = x<<O0_PIN;
}

uint8_t LOAD_T::get()
{
  return LOAD_ODR>>O0_PIN;
}