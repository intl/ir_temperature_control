#include <cstdint>
#include <cstdlib>
#include <iostm8s003f3.h>

class PID_t
{
public:
  struct pid_data_t
  {
  //! Last process value, used to find derivative of process value.
  int16_t lastProcessValue;
  //! Summation of errors, used for integrate calculations
  int32_t sumError;
  //! The Proportional tuning constant, multiplied with SCALING_FACTOR
  int16_t P_Factor;
  //! The Integral tuning constant, multiplied with SCALING_FACTOR
  int16_t I_Factor;
  //! The Derivative tuning constant, multiplied with SCALING_FACTOR
  int16_t D_Factor;
  //! Maximum allowed error, avoid overflow
  int16_t maxError;
  //! Maximum allowed sumerror, avoid overflow
  int32_t maxSumError;
  };
  static const uint8_t SCALING_FACTOR = 128;
  static const int16_t MAX_INT = 100;
  static const int32_t MAX_LONG = 0x7fffffffL;
  static const int32_t MAX_I_TERM = MAX_LONG/2;
  
  static const uint8_t KP = 72;
  static const uint8_t KI = 10;
  static const uint8_t KD = 64;
  
  pid_data_t pid;
  
  PID_t(int16_t p_factor = KP, int16_t i_factor = KI, int16_t d_factor = KD)
    {
    pid.sumError = 0;
    pid.lastProcessValue = 0;
    // Tuning constants for PID loop
    pid.P_Factor = p_factor;
    pid.I_Factor = i_factor;
    pid.D_Factor = d_factor;
    // Limits to avoid overflow
    pid.maxError = MAX_INT * (pid.P_Factor/10);
    pid.maxSumError = MAX_I_TERM / (pid.I_Factor + 1);
    }
  
 int16_t controller(int16_t setPoint, int16_t processValue);
 void reset_integrator();
};
